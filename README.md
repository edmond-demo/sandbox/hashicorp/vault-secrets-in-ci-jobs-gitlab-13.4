# Vault Secrets in CI Jobs - GitLab 13.4
### Released in GitLab 13.4
- [Using external secrets in CI](https://docs.gitlab.com/ee/ci/secrets/)
  - Uses the following CI variables:
    - VAULT_SERVER_URL (__REQUIRED__)
    - VAULT_AUTH_ROLE (optional but will likely fail on the [default role](https://www.vaultproject.io/api/auth/jwt#default_role))
    - VAULT_AUTH_PATH (optional)
- [Secrets in .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html#secrets)
- Roles used: https://gitlab.com/edmond-demo/sandbox/hashicorp/vault-example-from-gitlab-docs/-/issues/1
